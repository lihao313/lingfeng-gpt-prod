export default defineNuxtConfig({
  app: {
    cdnURL: 'https://static.sophoenix.com/',
    head: {
      title: "灵凤",
      meta: [
        {
          name: "description",
          content: "基于 OpenAI 的 ChatGPT 自然语言模型人工智能对话",
        },
      ],
    },
  },
  modules: ["@nuxtjs/tailwindcss", "@pinia/nuxt", "nuxt-icon"],
  css: [
    "highlight.js/styles/dark.css",
    "@/assets/style.less"
  ],
  tailwindcss: {
    config: {
      content: [],
      plugins: [require("@tailwindcss/typography")],
    },
  },
  ssr: false,
});
